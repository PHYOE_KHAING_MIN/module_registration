VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmTelexViewer 
   Caption         =   "TelexViewer"
   ClientHeight    =   6315
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13080
   Icon            =   "frmTelexViewer.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6315
   ScaleWidth      =   13080
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TAB_TlkTab 
      Height          =   780
      Left            =   8685
      TabIndex        =   9
      Top             =   5490
      Visible         =   0   'False
      Width           =   4200
      _Version        =   65536
      _ExtentX        =   7408
      _ExtentY        =   1376
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6773
      TabIndex        =   7
      Top             =   5640
      Width           =   1695
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4613
      TabIndex        =   6
      Top             =   5640
      Width           =   1695
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   12825
      Begin VB.ComboBox cmbFlights 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   1290
         Sorted          =   -1  'True
         TabIndex        =   8
         Text            =   "SQ 4711 XYZ"
         Top             =   225
         Width           =   2145
      End
      Begin VB.Label Label2 
         Caption         =   "Flight:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   300
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   12825
      Begin VB.TextBox txtTelexText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3495
         Left            =   2640
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   2
         Text            =   "frmTelexViewer.frx":030A
         Top             =   600
         Width           =   9930
      End
      Begin TABLib.TAB TAB_TelexViewer 
         Height          =   3525
         Left            =   240
         TabIndex        =   1
         Top             =   600
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   6227
         _StockProps     =   64
      End
      Begin VB.Label Label1 
         Caption         =   "Received telexes:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Width           =   2655
      End
   End
End
Attribute VB_Name = "frmTelexViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private AftUrno As New Collection

Private Sub cmbFlights_Change()
    'DisplayTelexTypes
End Sub

Private Sub cmbFlights_Click()
    DisplayTelexTypes
End Sub

Private Sub cmdClose_Click()
    Me.Hide
End Sub
'comment by kkh
'Private Sub cmdPrint_Click()
'    Dim ilCurrLine As Integer
'    ilCurrLine = TAB_TelexViewer.GetCurrentSelected
'    If ilCurrLine > -1 Then
'        Dim rptTelex As New rptTelex
'        rptTelex.Show , Me
'    Else
'        MsgBox "Please select a telex first!", vbInformation, "No telex selected"
'    End If
'End Sub

Private Sub Form_Load()
    Me.Hide
    InitTabTelexViewer
End Sub

Private Sub InitTabTelexViewer()
    TAB_TelexViewer.ResetContent
    TAB_TelexViewer.FontName = "Courier New"
    TAB_TelexViewer.HeaderFontSize = "16"
    TAB_TelexViewer.FontSize = "14"
    TAB_TelexViewer.SetTabFontBold True
    TAB_TelexViewer.EnableHeaderSizing True
    TAB_TelexViewer.LineHeight = 18
    TAB_TelexViewer.HeaderAlignmentString = "C,C"
    TAB_TelexViewer.ColumnAlignmentString = "L,L"
    TAB_TelexViewer.HeaderLengthString = "55,85"
    TAB_TelexViewer.HeaderString = "Type,Time"
    TAB_TelexViewer.LogicalFieldList = "Type,Time"
    
    'this is necessary to load the manually assigned telexes
    ' They are stored in the TLKTAB. The goal is to identify
    ' the TLXTAB urnos by the current flight urnos and read them
    ' from the TLXTAB to append the telex text field.
    TAB_TlkTab.ResetContent
    TAB_TlkTab.CedaPort = "3357"
    TAB_TlkTab.CedaHopo = frmData.strHOPO
    TAB_TlkTab.CedaCurrentApplication = gsAppName
    TAB_TlkTab.CedaTabext = frmData.strTableExt
    TAB_TlkTab.CedaUser = gsUserName
    TAB_TlkTab.CedaWorkstation = frmSplash.AATLoginControl1.GetWorkStationName
    TAB_TlkTab.CedaSendTimeout = "120"
    TAB_TlkTab.CedaReceiveTimeout = "240"
    TAB_TlkTab.CedaRecordSeparator = Chr(10)
    TAB_TlkTab.CedaIdentifier = gsAppName
    TAB_TlkTab.CedaServerName = frmData.strServer

    
    TAB_TlkTab.FontName = "Courier New"
    TAB_TlkTab.HeaderFontSize = "16"
    TAB_TlkTab.FontSize = "14"
    TAB_TlkTab.SetTabFontBold True
    TAB_TlkTab.EnableHeaderSizing True
    TAB_TlkTab.LineHeight = 18
    TAB_TlkTab.HeaderAlignmentString = "C,C"
    TAB_TlkTab.ColumnAlignmentString = "L,L"
    TAB_TlkTab.HeaderLengthString = "55,85"
    TAB_TlkTab.HeaderString = "FURN,TURN"
    TAB_TlkTab.LogicalFieldList = "FURN,TURN"
    
End Sub

Public Sub DisplayTelexInformation(sAftUrnoList As String)
    cmbFlights.Clear
    cmbFlights.Text = ""
    txtTelexText.Text = ""
    
    InitTabTelexViewer
    
    frmMain.StatusBar1.Panels(1).Text = "Loading Telex Data ..."
    Screen.MousePointer = vbHourglass

    
    FillFlightCombo (sAftUrnoList)
    LoadTelexes (sAftUrnoList)
    DisplayTelexTypes 'depends on the selection of the combo
    Me.Show

    Screen.MousePointer = vbDefault
    frmMain.StatusBar1.Panels(1).Text = "Ready."
End Sub
Private Function GetManuallyAssignedTlx(sAftUrnoList As String) As String
    Dim strWhere As String
    Dim strRet As String
    
    'First get the telex urnos which are manually assigned
    strWhere = "WHERE FURN IN (" + sAftUrnoList + ")"
    TAB_TlkTab.CedaAction "RT", "TLKTAB", "FURN,TURN", "", strWhere
    
    strRet = TAB_TlkTab.GetBufferByFieldList(0, TAB_TlkTab.GetLineCount - 1, "TURN", ",")
    GetManuallyAssignedTlx = strRet
    strRet = CStr(TAB_TlkTab.GetLineCount)
    'then read the telexes itself
End Function
Private Sub FillFlightCombo(sAftUrnoList As String)
    Dim strLineNo As String
    Dim strAftUrno As String
    Dim strFlno As String
    Dim i As Integer

    While AftUrno.count > 0
        AftUrno.Remove 1
    Wend

    For i = 1 To ItemCount(sAftUrnoList, ",") Step 1
        strAftUrno = GetItem(sAftUrnoList, i, ",")
        strLineNo = frmData.TabAFT.GetLinesByIndexValue("URNO", strAftUrno, 0)
        If IsNumeric(strLineNo) = True Then
            strFlno = frmData.TabAFT.GetFieldValue(CLng(strLineNo), "FLNO")
            If DoesKeyExist(AftUrno, strFlno) = False Then
                cmbFlights.AddItem strFlno
                AftUrno.Add strAftUrno, strFlno
            End If
        End If
    Next i
    If cmbFlights.ListCount > 0 Then
        cmbFlights.ListIndex = 0
    End If
End Sub

Private Sub LoadTelexes(sAftUrnoList As String)
    Dim strTlxUrnos As String
    
    'load regular telexes, which are automatically assigned to the flights
    frmData.LoadData frmData.TabTLX, "WHERE FLNU IN (" & sAftUrnoList & ")"
    'Get the urnos of telexes, which are manually assigned to flights
    strTlxUrnos = GetManuallyAssignedTlx(sAftUrnoList)
    If (strTlxUrnos <> "") Then
        frmData.LoadData frmData.TabTLX, "WHERE URNO IN (" & strTlxUrnos & ")", False
    End If
    
    frmData.TabTLX.IndexCreate "URNO", 0
End Sub

Private Sub DisplayTelexTypes()
    Dim i As Integer
    Dim ilIdx As Integer
    Dim strLineNos As String
    Dim strLineNo As String
    Dim strInsert As String
    Dim strTmp As String
    Dim dateTmp As Date
    Dim strAftUrno As String

    TAB_TelexViewer.ResetContent

    strAftUrno = AftUrno.Item(cmbFlights.Text)
'MWO: due to the fact that we must show all telexes, the manually assigned as well
'     we must iterate through the entire list in frmData.TabTLX

'MWO    ilIdx = GetItemNo(frmData.TabTLX.LogicalFieldList, "FLNU") - 1
'MWO    strLineNos = frmData.TabTLX.GetLinesByColumnValue(CLng(ilIdx), strAftUrno, 0)

    For i = 0 To frmData.TabTLX.GetLineCount - 1
        strInsert = frmData.TabTLX.GetFieldValue(i, "TTYP") & ","
        strTmp = frmData.TabTLX.GetFieldValue(i, "TIME") & ","
        If Len(strTmp) > 0 Then
            dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
            strInsert = strInsert & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD")
        End If
        TAB_TelexViewer.InsertTextLine strInsert, True
        TAB_TelexViewer.SetLineTag TAB_TelexViewer.GetLineCount - 1, frmData.TabTLX.GetFieldValue(i, "URNO")
    Next i
'    For i = 1 To ItemCount(strLineNos, ",") Step 1
'        strLineNo = GetItem(strLineNos, i, ",")
'        strInsert = frmData.TabTLX.GetFieldValue(CLng(strLineNo), "TTYP") & ","
'        strTmp = frmData.TabTLX.GetFieldValue(CLng(strLineNo), "TIME") & ","
'        If Len(strTmp) > 0 Then
'            dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
'            strInsert = strInsert & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD")
'        End If
'        TAB_TelexViewer.InsertTextLine strInsert, True
'        TAB_TelexViewer.SetLineTag TAB_TelexViewer.GetLineCount - 1, frmData.TabTLX.GetFieldValue(CLng(strLineNo), "URNO")
'    Next i
'MWO: END
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Cancel = 1
    Me.Hide
End Sub

Private Sub TAB_TelexViewer_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo > -1 Then
        Dim strTlxUrno As String
        Dim strLineNo As String
        Dim strText As String
        
        strTlxUrno = TAB_TelexViewer.GetLineTag(LineNo)
        strLineNo = frmData.TabTLX.GetLinesByIndexValue("URNO", strTlxUrno, 0)
        If IsNumeric(strLineNo) = True Then
            strText = frmData.TabTLX.GetFieldValues(CLng(strLineNo), "TXT1")
            strText = strText & frmData.TabTLX.GetFieldValues(CLng(strLineNo), "TXT2")
            txtTelexText.Text = CleanString(strText, FOR_CLIENT, True)
        End If
    End If
End Sub

Function DoesKeyExist(ByRef refCollection As Collection, refKey As String) As Boolean
    On Error GoTo DoesNotExist
    refCollection.Item (refKey)
    DoesKeyExist = True
    Exit Function
DoesNotExist:
    DoesKeyExist = False
    Err.Clear
End Function
