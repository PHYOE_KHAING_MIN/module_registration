Attribute VB_Name = "mdlMain"
Option Explicit

Global gsAppName As String
Global gsUserName As String
Global gsTplUrno As String

Global gbDebug As Boolean
Global gbTeamLeaderGroup As Boolean
Global gsDebugToday As String

Global sglUTCOffsetHours As Single

Sub Main()
    Dim strRetLogin As String
    Dim strCommand As String

    gsAppName = "iPaxApplication"
    strCommand = Command()
    If UCase(GetItem(strCommand, 1, ",")) = "DEBUG" Then
        gbDebug = True
        Dim strTime As String
        strTime = GetItem(strCommand, 3, ",")
        If Len(strTime) > 0 And IsDate(strTime) = True Then
             Date = strTime
        End If
    Else
        gbDebug = False
    End If

    'do the login
    InitLoginControl
    
                        If gbDebug = False Then
        strRetLogin = frmSplash.AATLoginControl1.ShowLoginDialog
    Else
        strRetLogin = "OK"
    End If

    ' close connection to server
    frmSplash.UfisCom1.CleanupCom

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        End
    Else
        ' save the user name
        gsUserName = frmSplash.AATLoginControl1.GetUserName()

        ' load the data form (not visible)
        Load frmData

        ' check if we are running in debug mode
        If gbDebug = True Then
            gsUserName = GetItem(strCommand, 2, ",")
            frmData.Visible = True
        End If

        ' load the data and create the views
'        frmData.InitialLoad True, True

        ' show the main dialog
        
        MsgBox " Registred. Exiting"
        End
        
'        frmMain.Show
    End If
End Sub

Private Sub InitLoginControl()

    '''***** BEFORE YOU ADDED NEW FUNCTION CHECK AT THE DB WITH THIS SCRIPT
    Dim strRegister As String
    ' giving the login control an UfisCom to do the login
    Set frmSplash.AATLoginControl1.UfisComCtrl = frmSplash.UfisCom1

    ' setting some information to be displayed on the login control
    'frmSplash.AATLoginControl1.ApplicationName = gsAppName
    frmSplash.AATLoginControl1.ApplicationName = "GDVV"
    frmSplash.AATLoginControl1.VersionString = "5.2.0.1"
    frmSplash.AATLoginControl1.InfoCaption = "Application Registration"
    frmSplash.AATLoginControl1.InfoButtonVisible = True
    frmSplash.AATLoginControl1.InfoUfisVersion = "UFIS Version 5.2"
    frmSplash.AATLoginControl1.InfoAppVersion = CStr("InfoPC 5.2.0.1")
    frmSplash.AATLoginControl1.InfoCopyright = "� 2001-2005 Amadeus UFIS"
    frmSplash.AATLoginControl1.InfoAAT = "Amadeus UFIS"
    frmSplash.AATLoginControl1.UserNameLCase = False 'not automatic upper case letters
    frmSplash.AATLoginControl1.ApplicationName = "GDVV"
    frmSplash.AATLoginControl1.LoginAttempts = 3

    ' build the register string for BDPS-SEC
    strRegister = "GDVV,InitModu,InitModu,Initialize (InitModu),B,1"
    'strRegister = strRegister & ",GDVV Web,Web_Access,Gate Changes Aurhorization,Z,0"
    strRegister = strRegister & ",General,Z_GDVV,GDV Access,Z,1"
    strRegister = strRegister & ",GDVV_SubFunctions,Web_Access,Gate Changes Aurhorization,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Flight_View,Flight View,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Flight_Detail_View,Flight Detail View,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Job_List,Job List,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Job_Detail_View,Job Detail View,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Gate_Arr_Main_Activity,Gate Arr Main Activity,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Track_Sheet,Track Sheet,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Gate_Arr_Remarks,Gate Arr Remarks,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Lost_And_Found,Lost And Found,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Gate_Arr_Report_Submit,Gate Arr Report Submit,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Pre_Flight,Pre-Flight,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,CSA_Counter,CSA Counter,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Counter_Dep_Main_Activity,Counter Dep Main Activity,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Meal,Meal,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,SSS,SSS,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Counter_Dep_Remarks,Counter Dep Remarks,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Seat_Changes_or_Upgrading,Seat Changes or Upgrading,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Closing_Main_Activities,Closing Main Activities,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Counter_Dep_LMC,Counter Departure LMC,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Flight_Coupon,Flight Coupon,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Counter_Dep_Report_Submit,Counter Dep Report Submit,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Gate_Dep_Main_Activities,Gate Dep Main Activities,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Gate_Dep_LMC,Gate Departure LMC,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,ICAO,ICAO,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Delay,Delay,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Additional_Info,Additional Info,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,SSS_Cases,SSS Cases,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,Gate_Dep_Remarks,Gate Dep Remarks,Z,0"
'    strRegister = strRegister & ",iPaxApplication_Web,GatexDep_Report_Submit,Gate Dep Report Submit,Z,0"
    
    frmSplash.AATLoginControl1.RegisterApplicationString = strRegister
    Debug.Print strRegister
    
End Sub

Public Sub ExitApplication()
    End
End Sub
